# Transmission #

Transmission is a reddit client that serves as a test bed for learning, while also solves some of my usability grievences with other popular reddit clients.

## Things I'm Learning ##

* MVP architecture
* OAuth2 
* Kotlin
* Retrofit
* Moshi
* Mockito
* CircleCI

## Things I'm Hoping to Learn ##

* Implementing a customizable UX
* RXJava2