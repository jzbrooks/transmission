package com.jzbrooks.transmission;

/**
 * Implemented by all presenters in the system
 */
public interface BasePresenter {
    void start();
}
