package com.jzbrooks.transmission.data.source.remote;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.jzbrooks.transmission.data.Message;
import com.jzbrooks.transmission.data.Post;
import com.jzbrooks.transmission.data.Subreddit;
import com.jzbrooks.transmission.data.User;
import com.jzbrooks.transmission.data.source.RedditDataSource;
import com.jzbrooks.transmission.data.source.remote.api.FetchRedditPostsTask;
import com.jzbrooks.transmission.data.source.remote.api.FetchSubredditsTask;
import com.jzbrooks.transmission.data.source.remote.api.auth.AuthenticateAsync;
import com.jzbrooks.transmission.data.source.remote.api.auth.RefreshAccessTokenTask;

import java.util.List;

/**
 * Implementation of the data source that makes http requests against Reddit's API
 */
public class RedditRemoteDataSource implements RedditDataSource {
    private final static String LOG_TAG = RedditRemoteDataSource.class.getSimpleName();

    private static RedditRemoteDataSource instance;

    /**
     * Remote model operations for reddit data.
     * @return the single instance of {@link RedditRemoteDataSource}
     */
    public static RedditRemoteDataSource getInstance() {

        if (instance == null) {
            instance = new RedditRemoteDataSource();
        }

        return instance;
    }

    // prevent direct instantiation
    private RedditRemoteDataSource() { }

    @Override
    public void signIn(@NonNull String redirectUrl, @NonNull LoginUserCallback callback) {

        AuthenticateAsync loginTask = new AuthenticateAsync();
        loginTask.setCallback(callback);
        loginTask.execute(redirectUrl);
    }

    @Override
    public void storeUser(@NonNull User user) {
        // local source will handle this
    }

    @Override
    public void updateAccessToken(@NonNull User user, @NonNull LoginUserCallback callback) {

        RefreshAccessTokenTask task = new RefreshAccessTokenTask();
        task.setCallback(callback);
        task.execute(user);
    }

    @Override
    public void getCurrentUser(@NonNull FetchUserCallback callback) {

    }

    @Override
    public void setCurrentUser(@NonNull User user) {

    }

    @Override
    public void signOut(@NonNull User user) {

    }

    @Override
    public void getPosts(@Nullable User currentUser,
                         @Nullable final String currentSubreddit,
                         @NonNull final String lastLoadedId,
                         @NonNull LoadPostsCallback callback) {


        final FetchRedditPostsTask task = new FetchRedditPostsTask();
        task.setCallback(callback);

        if (currentUser != null) {
            final String accessToken = currentUser.getAccessToken();
            if (accessToken == null) {
                updateAccessToken(currentUser, new LoginUserCallback() {
                    @Override
                    public void onLogin(User user) {

                        task.execute(user.getAccessToken(), currentSubreddit, lastLoadedId);
                    }

                    @Override
                    public void onFailure(String errorMessage) {
                        task.execute(null, currentSubreddit, lastLoadedId);
                        Log.e(LOG_TAG, errorMessage);
                    }
                });
            } else {
                task.execute(currentUser.getAccessToken(), currentSubreddit, lastLoadedId);
            }
        } else {
            task.execute(null, currentSubreddit, lastLoadedId);
        }
    }

    @Override
    public void getPost(@NonNull String postId, @NonNull GetPostCallback callback) {

    }

    @Override
    public void submitPost(@NonNull Post post) {

    }

    @Override
    public void cachePosts(@NonNull List<Post> posts) {

    }

    @Override
    public void refreshPosts() {

    }

    @Override
    public void deleteAllPosts() {

    }

    @Override
    public void getMessages(@NonNull LoadMessagesCallback callback) {

    }

    @Override
    public void getMessage(@NonNull String messageId, @NonNull GetMessageCallback callback) {

    }

    @Override
    public void saveMessage(@NonNull Message message) {

    }

    @Override
    public void refreshMessages() {

    }

    @Override
    public void deleteAllMessages() {

    }

    @Override
    public void getSubreddits(@Nullable User user, @NonNull LoadSubredditsCallback callback) {

        final FetchSubredditsTask task = new FetchSubredditsTask();
        task.setCallback(callback);

        if (user != null) {
            final String accessToken = user.getAccessToken();
            if (accessToken == null) {
                updateAccessToken(user, new LoginUserCallback() {
                    @Override
                    public void onLogin(User user) {

                        task.execute(user.getAccessToken());
                    }

                    @Override
                    public void onFailure(String errorMessage) {
                        task.execute(new String[] { null });
                        Log.e(LOG_TAG, errorMessage);
                    }
                });
            } else {
                task.execute(user.getAccessToken());
            }
        } else {
            task.execute(new String[] { null });
        }
    }

    @Override
    public void getSubreddit(@NonNull String subredditId, @NonNull GetSubredditCallback callback) {

    }

    @Override
    public void cacheSubreddits(@NonNull List<Subreddit> subreddits) {

        throw new UnsupportedOperationException("Caching is handled by the local source.");
    }

    @Override
    public void subscribeToSubreddit(@NonNull Subreddit subreddit) {

    }

    @Override
    public void refreshSubreddits() {

    }

    @Override
    public void deleteAllSubreddits() {

    }
}
