package com.jzbrooks.transmission.data;

import android.support.annotation.NonNull;

/**
 * Model of a reddit user
 */
public class User {
    private String username;
    private String refreshToken;
    private String accessToken;

    public String getUsername() {
        return username;
    }

    public void setUsername(@NonNull String username) {
        this.username = username;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(@NonNull String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
