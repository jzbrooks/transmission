package com.jzbrooks.transmission.data.source.remote.api.responses;

/**
 * Listing child data from the reddit api
 */
public final class ChildData {
    public String id;
    public String display_name;
    public String subreddit;
    public String author;
    public String title;
    public String domain;
    public String url;
    public String selftext;
    public String selftext_html;
    public long created;
    public long num_comments;
    public long score;
    public boolean visited;
    public boolean over_18;
}
