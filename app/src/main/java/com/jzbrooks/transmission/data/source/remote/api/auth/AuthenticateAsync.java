package com.jzbrooks.transmission.data.source.remote.api.auth;

import android.net.Uri;
import android.os.AsyncTask;

import com.jzbrooks.transmission.data.User;
import com.jzbrooks.transmission.data.source.RedditDataSource;
import com.jzbrooks.transmission.data.source.remote.ApiUtilities;
import com.jzbrooks.transmission.data.source.remote.api.RedditApi;
import com.jzbrooks.transmission.data.source.remote.api.RedditAuthApi;
import com.jzbrooks.transmission.data.source.remote.api.responses.AccountResponse;
import com.jzbrooks.transmission.data.source.remote.api.responses.AuthResponse;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

/**
 * Authentication task for reddit users
 */
public final class AuthenticateAsync extends AsyncTask<Object, Void, AuthenticationResult> {
    private RedditDataSource.LoginUserCallback callback;

    @Override
    protected AuthenticationResult doInBackground(Object... params) {
        final int VALID_ARGS_LEN = 1;

        if (params == null || params.length != VALID_ARGS_LEN) {
            throw new IllegalArgumentException(
                    String.format("must specify correct number of arguments %s", VALID_ARGS_LEN));
        }

        if (!(params[0] instanceof String)) {
            throw new IllegalArgumentException("The first argument to AuthenticateAsync execute " +
                    "must be the reddit oauth redirect url given after a user grants access.");
        }

        String url = (String)params[0];
        String code = Uri.parse(url).getQueryParameter("code");
        AuthenticationResult authenticationResult = new AuthenticationResult();

        try {
            //authorize
            Retrofit signOnRetrofit = new Retrofit.Builder()
                    .baseUrl(RedditAuthApi.AUTH_BASE_URL)
                    .client(ApiUtilities.Companion.getSignInClient(code))
                    .addConverterFactory(MoshiConverterFactory.create())
                    .build();

            RedditAuthApi signOnApi = signOnRetrofit.create(RedditAuthApi.class);

            Call<AuthResponse> authResponseCall = signOnApi.getAccessToken();
            Response<AuthResponse> signOnResponse = authResponseCall.execute();
            if (signOnResponse.code() != 200) {
                authenticationResult.setErrorMessage("There was a problem getting an access token.");
                return authenticationResult;
            }

            AuthResponse authResponse = signOnResponse.body();
            String refreshToken = authResponse.refresh_token;
            String accessToken = authResponse.access_token;

            //get identity information
            Retrofit authenticatedRetrofit = new Retrofit.Builder()
                    .baseUrl(RedditApi.BASE_URL)
                    .client(ApiUtilities.Companion.getAuthenticatedClient(accessToken))
                    .addConverterFactory(MoshiConverterFactory.create())
                    .build();

            RedditApi redditApi = authenticatedRetrofit.create(RedditApi.class);

            Call<AccountResponse> call = redditApi.getIdentity();
            Response<AccountResponse> userResponse = call.execute();
            if (userResponse.code() != 200) {
                authenticationResult.setErrorMessage(
                        "There was a problem getting reddit user identity info."
                );
                return authenticationResult;
            }

            AccountResponse accountResponse = userResponse.body();
            String username = accountResponse.name;

            if (username != null && refreshToken != null) {
                User loggedInUser = new User();
                loggedInUser.setUsername(username);
                loggedInUser.setRefreshToken(refreshToken);
                loggedInUser.setAccessToken(accessToken);
                authenticationResult.setUser(loggedInUser);
            }

        } catch (IOException ex) {
            authenticationResult.setErrorMessage(ex.getMessage());
        }

        return authenticationResult;
    }

    @Override
    public void onPostExecute(AuthenticationResult authResult) {
        User user = authResult.getUser();
        String errorMessage = authResult.getErrorMessage();

        if (user != null && errorMessage == null) {
            callback.onLogin(user);
        } else {
            callback.onFailure(errorMessage);
        }
    }

    public void setCallback(RedditDataSource.LoginUserCallback callback) {
        this.callback = callback;
    }
}
