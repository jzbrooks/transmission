package com.jzbrooks.transmission.data.source;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.jzbrooks.transmission.data.Message;
import com.jzbrooks.transmission.data.Post;
import com.jzbrooks.transmission.data.Subreddit;
import com.jzbrooks.transmission.data.User;

import java.util.List;

/**
 * Interface definition for reddit data sources.
 * This should be implemented by all data sources.
 */
public interface RedditDataSource {

    // Reddit Authentication
    interface LoginUserCallback {

        void onLogin(User user);

        void onFailure(String errorMessage);
    }

    interface FetchUserCallback {

        void onSuccess(User user);

        void onFailure();
    }

    void signIn(@NonNull String redirectUrl, @NonNull LoginUserCallback callback);

    void storeUser(@NonNull User user);

    void updateAccessToken(@NonNull User user, @NonNull LoginUserCallback callback);

    void getCurrentUser(@NonNull FetchUserCallback callback);

    void setCurrentUser(@NonNull User user);

    void signOut(@NonNull User user);

    // Reddit Posts
    interface LoadPostsCallback {

        void onPostsLoaded(List<Post> posts);

        void onDataNotAvailable();
    }

    interface GetPostCallback {

        void onPostLoaded(Post post);

        void onDataNotAvailable();
    }

    void getPosts(@Nullable User currentUser,
                  @Nullable String currentSubreddit,
                  @NonNull String lastLoadedId,
                  @NonNull LoadPostsCallback callback);

    void getPost(@NonNull String postId, @NonNull GetPostCallback callback);

    void submitPost(@NonNull Post post);

    void cachePosts(@NonNull List<Post> posts);

    void refreshPosts();

    void deleteAllPosts();

    // Reddit Messages
    interface LoadMessagesCallback {

        void onMessagesLoaded(List<Message> messages);

        void onDataNotAvailable();
    }

    interface GetMessageCallback {

        void onMessageLoaded(Message message);

        void onDataNotAvailable();
    }

    void getMessages(@NonNull LoadMessagesCallback callback);

    void getMessage(@NonNull String messageId, @NonNull GetMessageCallback callback);

    void saveMessage(@NonNull Message message);

    void refreshMessages();

    void deleteAllMessages();

    // Reddit Subreddits
    interface LoadSubredditsCallback {

        void onSubredditsLoaded(List<Subreddit> subreddits);

        void onDataNotAvailable();
    }

    interface GetSubredditCallback {

        void onSubredditLoaded(Subreddit subreddit);

        void onDataNotAvailable();
    }

    void getSubreddits(@Nullable User user, @NonNull LoadSubredditsCallback callback);

    void getSubreddit(@NonNull String subredditId, @NonNull GetSubredditCallback callback);

    void cacheSubreddits(@NonNull List<Subreddit> subreddits);

    void subscribeToSubreddit(@NonNull Subreddit subreddit);

    void refreshSubreddits();

    void deleteAllSubreddits();
}
