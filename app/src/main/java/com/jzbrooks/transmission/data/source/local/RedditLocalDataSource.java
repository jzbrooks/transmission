package com.jzbrooks.transmission.data.source.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.jzbrooks.transmission.data.Message;
import com.jzbrooks.transmission.data.Post;
import com.jzbrooks.transmission.data.Subreddit;
import com.jzbrooks.transmission.data.User;
import com.jzbrooks.transmission.data.source.RedditDataSource;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the local data source that makes requests to a SQLite database
 */
public class RedditLocalDataSource implements RedditDataSource {

    private static final String LOG_TAG = RedditLocalDataSource.class.getSimpleName();

    private static RedditLocalDataSource instance;

    private RedditDatabaseHelper databaseHelper;
    private User currentUser;

    /**
     * Local model storage for reddit data. Largely used as a cache.
     * @param context the android application context
     * @return the single instance of {@link RedditLocalDataSource}
     */
    public static RedditLocalDataSource getInstance(@NonNull Context context) {

        if (instance == null) {
            instance = new RedditLocalDataSource(context);
        }

        return instance;
    }

    private RedditLocalDataSource(@NonNull Context context) {

        databaseHelper = new RedditDatabaseHelper(context);
    }

    @Override
    public void signIn(@NonNull String redirectUrl, @NonNull LoginUserCallback callback) {
        // the remote api handles this
    }

    @Override
    public void storeUser(@NonNull User user) {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(RedditContract.UserEntry.COLUMN_USERNAME, user.getUsername());
        values.put(RedditContract.UserEntry.COLUMN_REFRESH_TOKEN, user.getRefreshToken());

        long newRowId = database.insert(RedditContract.UserEntry.TABLE_NAME, null, values);

        if (newRowId < 0) {
            Log.e(LOG_TAG, String.format("Error inserting user - (%s, %s) into the database.",
                    user.getUsername(),
                    user.getRefreshToken()
            ));
        }
    }

    @Override
    public void updateAccessToken(@NonNull User user, @NonNull LoginUserCallback callback) {
        // the remote api handles this
    }

    @Override
    public void getCurrentUser(@NonNull FetchUserCallback callback) {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();

        String[] projection = {
            RedditContract.UserEntry._ID,
            RedditContract.UserEntry.COLUMN_USERNAME,
            RedditContract.UserEntry.COLUMN_REFRESH_TOKEN
        };

        String selection = null;
        String[] selectionArgs = null;

        if (currentUser != null) {
            selection = RedditContract.UserEntry.COLUMN_USERNAME + " = ?";
            selectionArgs = new String[] {currentUser.getUsername()};
        }

        Cursor cursor = null;
        User user = new User();
        try {

            cursor = database.query(
                    RedditContract.UserEntry.TABLE_NAME,
                    projection,
                    selection,
                    selectionArgs,
                    null,
                    null,
                    null
            );

            if (!cursor.moveToFirst()) {
                callback.onFailure();
                return;
            }

            String username = cursor.getString(
                    cursor.getColumnIndex(RedditContract.UserEntry.COLUMN_USERNAME)
            );
            user.setUsername(username);

            String refreshToken = cursor.getString(
                    cursor.getColumnIndex(RedditContract.UserEntry.COLUMN_REFRESH_TOKEN)
            );
            user.setRefreshToken(refreshToken);

            callback.onSuccess(user);

        } finally {

            if (cursor != null) cursor.close();
            database.close();
        }
    }

    @Override
    public void setCurrentUser(@NonNull User user) {
        currentUser = user;
    }

    @Override
    public void signOut(@NonNull User user) {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();

        try {
            String selection = RedditContract.UserEntry.COLUMN_USERNAME + " LIKE ?";
            String[] selectionArgs = {user.getUsername()};

            database.delete(RedditContract.UserEntry.TABLE_NAME, selection, selectionArgs);
        } finally {
            database.close();
        }
    }

    @Override
    public void getPosts(@Nullable User currentUser,
                         @Nullable String subredditName,
                         @NonNull String lastLoadedId,
                         @NonNull LoadPostsCallback callback) {

        SQLiteDatabase database = databaseHelper.getWritableDatabase();
        Cursor cursor = null;

        String[] projection = {
                RedditContract.PostEntry._ID,
                RedditContract.PostEntry.COLUMN_API_ID,
                RedditContract.PostEntry.COLUMN_TITLE,
                RedditContract.PostEntry.COLUMN_SUBREDDIT_NAME,
                RedditContract.PostEntry.COLUMN_AUTHOR,
                RedditContract.PostEntry.COLUMN_DOMAIN,
                RedditContract.PostEntry.COLUMN_URL,
                RedditContract.PostEntry.COLUMN_SELFTEXT,
                RedditContract.PostEntry.COLUMN_SELFTEXT_HTML,
                RedditContract.PostEntry.COLUMN_CREATED,
                RedditContract.PostEntry.COLUMN_COMMENT_COUNT,
                RedditContract.PostEntry.COLUMN_SCORE,
                RedditContract.PostEntry.COLUMN_NSFW,
                RedditContract.PostEntry.COLUMN_VISITED
        };

        String selection = null;
        String[] selectionArgs = null;

        if (subredditName != null) {
            selection = RedditContract.PostEntry.COLUMN_SUBREDDIT_NAME + " = ?";
            selectionArgs = new String[] { subredditName };
        }

        try {
            cursor = database.query(
                    RedditContract.PostEntry.TABLE_NAME,
                    projection,
                    selection,
                    selectionArgs,
                    null,
                    null,
                    null
            );

            if (!cursor.moveToFirst()) {
                callback.onDataNotAvailable();
            } else {
                List<Post> posts = new ArrayList<>(25);
                do {
                    String id = cursor.getString(
                            cursor.getColumnIndex(RedditContract.PostEntry.COLUMN_API_ID)
                    );
                    String title = cursor.getString(
                            cursor.getColumnIndex(RedditContract.PostEntry.COLUMN_TITLE)
                    );
                    String subreddit = cursor.getString(
                            cursor.getColumnIndex(RedditContract.PostEntry.COLUMN_SUBREDDIT_NAME)
                    );
                    String author = cursor.getString(
                            cursor.getColumnIndex(RedditContract.PostEntry.COLUMN_AUTHOR)
                    );
                    String domain = cursor.getString(
                            cursor.getColumnIndex(RedditContract.PostEntry.COLUMN_DOMAIN)
                    );
                    String url = cursor.getString(
                            cursor.getColumnIndex(RedditContract.PostEntry.COLUMN_URL)
                    );
                    String selfText = cursor.getString(
                            cursor.getColumnIndex(RedditContract.PostEntry.COLUMN_SELFTEXT)
                    );
                    String selfTextHtml = cursor.getString(
                            cursor.getColumnIndex(RedditContract.PostEntry.COLUMN_SELFTEXT_HTML)
                    );
                    long created = cursor.getLong(
                            cursor.getColumnIndex(RedditContract.PostEntry.COLUMN_CREATED)
                    );
                    long commentCount = cursor.getLong(
                            cursor.getColumnIndex(RedditContract.PostEntry.COLUMN_COMMENT_COUNT)
                    );
                    long score = cursor.getLong(
                            cursor.getColumnIndex(RedditContract.PostEntry.COLUMN_SCORE)
                    );
                    boolean nsfw = cursor.getInt(
                            cursor.getColumnIndex(RedditContract.PostEntry.COLUMN_NSFW)
                    ) == 1;
                    boolean visited = cursor.getInt(
                            cursor.getColumnIndex(RedditContract.PostEntry.COLUMN_VISITED)
                    ) == 1;

                    Post post = new Post.Builder()
                            .id(id)
                            .title(title)
                            .subreddit(subreddit)
                            .author(author)
                            .domain(domain)
                            .url(url)
                            .selftext(selfText)
                            .selftextHtml(selfTextHtml)
                            .created(created)
                            .commentCount(commentCount)
                            .score(score)
                            .visited(visited)
                            .overEighteen(nsfw)
                            .build();

                    posts.add(post);

                } while(cursor.moveToNext());

                callback.onPostsLoaded(posts);
            }

        } finally {
            if (cursor != null) cursor.close();
            database.close();
        }
    }

    @Override
    public void getPost(@NonNull String postId, @NonNull GetPostCallback callback) {

    }

    @Override
    public void cachePosts(@NonNull List<Post> posts) {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();

        try {

            for (Post post : posts) {
                ContentValues values = new ContentValues();

                values.put(RedditContract.PostEntry.COLUMN_API_ID, post.getId());
                values.put(RedditContract.PostEntry.COLUMN_TITLE, post.getTitle());
                values.put(RedditContract.PostEntry.COLUMN_SUBREDDIT_NAME, post.getSubreddit());
                values.put(RedditContract.PostEntry.COLUMN_AUTHOR, post.getAuthor());
                values.put(RedditContract.PostEntry.COLUMN_DOMAIN, post.getDomain());
                values.put(RedditContract.PostEntry.COLUMN_URL, post.getUrl());
                values.put(RedditContract.PostEntry.COLUMN_SELFTEXT, post.getSelftext());
                values.put(RedditContract.PostEntry.COLUMN_SELFTEXT_HTML, post.getSelftextHtml());
                values.put(RedditContract.PostEntry.COLUMN_CREATED, post.getCreated());
                values.put(RedditContract.PostEntry.COLUMN_SCORE, post.getScore());
                values.put(RedditContract.PostEntry.COLUMN_COMMENT_COUNT, post.getCommentCount());
                values.put(RedditContract.PostEntry.COLUMN_VISITED, post.isVisited());
                values.put(RedditContract.PostEntry.COLUMN_NSFW, post.isOverEighteen());

                long newRowId = database.insert(RedditContract.PostEntry.TABLE_NAME, null, values);

                if (newRowId < 0) {
                    Log.e(LOG_TAG, String.format("Error inserting post - (/r/%s, %s) into the database.",
                            post.getSubreddit(),
                            post.getTitle()
                    ));
                }
            }
        } finally {
            database.close();
        }
    }

    @Override
    public void submitPost(@NonNull Post post) {

    }

    @Override
    public void refreshPosts() {

    }

    @Override
    public void deleteAllPosts() {

    }

    @Override
    public void getMessages(@NonNull LoadMessagesCallback callback) {

    }

    @Override
    public void getMessage(@NonNull String messageId, @NonNull GetMessageCallback callback) {

    }

    @Override
    public void saveMessage(@NonNull Message message) {

    }

    @Override
    public void refreshMessages() {

    }

    @Override
    public void deleteAllMessages() {

    }

    @Override
    public void getSubreddits(@Nullable User user, @NonNull LoadSubredditsCallback callback) {

    }

    @Override
    public void getSubreddit(@NonNull String subredditId, @NonNull GetSubredditCallback callback) {

    }

    @Override
    public void cacheSubreddits(@NonNull List<Subreddit> subreddits) {
        
    }

    @Override
    public void subscribeToSubreddit(@NonNull Subreddit subreddit) {

    }

    @Override
    public void refreshSubreddits() {

    }

    @Override
    public void deleteAllSubreddits() {

    }

    @Nullable
    private String subredditIdByName(@NonNull String subredditName) {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();

        String[] projection = {
                RedditContract.SubredditEntry.COLUMN_API_ID
        };

        String selection = RedditContract.SubredditEntry.COLUMN_SUBREDDIT_NAME + " = ?";
        String[] selectionArgs = { subredditName };

        Cursor cursor = null;
        String subredditId = null;
        try {

            cursor = database.query(
                    RedditContract.SubredditEntry.TABLE_NAME,
                    projection,
                    selection,
                    selectionArgs,
                    null,
                    null,
                    null
            );

            if (cursor.getCount() != 1 || cursor.moveToFirst()) {
                subredditId = cursor.getString(
                        cursor.getColumnIndex(RedditContract.SubredditEntry.COLUMN_API_ID)
                );
            }
        } finally {

            if (cursor != null) cursor.close();
            database.close();
        }

        return subredditId;
    }
}
