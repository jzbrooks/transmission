package com.jzbrooks.transmission.data.source;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.jzbrooks.transmission.data.Message;
import com.jzbrooks.transmission.data.Post;
import com.jzbrooks.transmission.data.Subreddit;
import com.jzbrooks.transmission.data.User;

import java.util.List;

/**
 * Reddit data repository abstraction layer that handles
 * delegating to local and remote data sources
 */
public class RedditDataStore implements RedditDataSource {

    private static final String LOG_TAG = RedditDataStore.class.getSimpleName();
    private static RedditDataStore instance;

    private RedditDataSource localDataSource;
    private RedditDataSource remoteDataSource;

    // Prevent direct instantiation
    private RedditDataStore(@NonNull RedditDataSource remote, @NonNull RedditDataSource local) {

        remoteDataSource = remote;
        localDataSource = local;
    }

    /**
     * Returns the single instance of this class, or creates it if necessary
     *
     * @param remote the reddit web api data source wrapper
     * @param local the device storage data source wrapper
     * @return the {@link RedditDataStore} instance
     */
    public static RedditDataStore getInstance(RedditDataSource remote, RedditDataSource local) {

        if (instance == null) {
            instance = new RedditDataStore(remote, local);
        }

        return instance;
    }

    @Override
    public void signIn(@NonNull String redirectUrl, @NonNull LoginUserCallback callback) {

        remoteDataSource.signIn(redirectUrl, callback);
    }

    @Override
    public void storeUser(@NonNull User user) {

        localDataSource.storeUser(user);
    }

    @Override
    public void updateAccessToken(@NonNull final User user, @NonNull final LoginUserCallback callback) {

        remoteDataSource.updateAccessToken(user, new LoginUserCallback() {
            @Override
            public void onLogin(User user) {

                localDataSource.storeUser(user);

                callback.onLogin(user);
            }

            @Override
            public void onFailure(String errorMessage) {
                Log.e(LOG_TAG, errorMessage);
            }
        });
    }

    @Override
    public void getCurrentUser(@NonNull FetchUserCallback callback) {

        localDataSource.getCurrentUser(callback);
    }

    @Override
    public void setCurrentUser(@NonNull User user) {

        localDataSource.setCurrentUser(user);
    }

    @Override
    public void signOut(@NonNull User user) {

        localDataSource.signOut(user);
        remoteDataSource.signOut(user);
    }

    @Override
    public void getPosts(@Nullable final User currentUser,
                         @Nullable final String currentSubreddit,
                         @NonNull final String lastLoadedId,
                         @NonNull final LoadPostsCallback callback) {

        remoteDataSource.getPosts(currentUser, currentSubreddit, lastLoadedId,
                new LoadPostsCallback() {

                @Override
                public void onPostsLoaded(List<Post> posts) {

                    localDataSource.cachePosts(posts);
                    callback.onPostsLoaded(posts);
                }

                @Override
                public void onDataNotAvailable() {

                    localDataSource.getPosts(currentUser, currentSubreddit, lastLoadedId, callback);
                }
        });
    }

    @Override
    public void getPost(@NonNull String postId, @NonNull GetPostCallback callback) {

    }

    @Override
    public void submitPost(@NonNull Post post) {

    }

    @Override
    public void cachePosts(@NonNull List<Post> posts) {

        throw new UnsupportedOperationException("Caching is handled by the data tier.");
    }

    @Override
    public void refreshPosts() {

    }

    @Override
    public void deleteAllPosts() {
        localDataSource.deleteAllPosts();
        remoteDataSource.deleteAllPosts();
    }

    @Override
    public void getMessages(@NonNull LoadMessagesCallback callback) {

    }

    @Override
    public void getMessage(@NonNull String messageId, @NonNull GetMessageCallback callback) {

    }

    @Override
    public void saveMessage(@NonNull Message message) {

    }

    @Override
    public void refreshMessages() {

    }

    @Override
    public void deleteAllMessages() {

    }

    @Override
    public void getSubreddits(@Nullable final User user, @NonNull final LoadSubredditsCallback callback) {

        remoteDataSource.getSubreddits(user, new LoadSubredditsCallback() {
            @Override
            public void onSubredditsLoaded(List<Subreddit> subreddits) {

                localDataSource.cacheSubreddits(subreddits);
                callback.onSubredditsLoaded(subreddits);
            }

            @Override
            public void onDataNotAvailable() {

                localDataSource.getSubreddits(user, callback);
            }
        });
    }

    @Override
    public void getSubreddit(@NonNull String subredditId, @NonNull GetSubredditCallback callback) {

    }

    @Override
    public void cacheSubreddits(@NonNull List<Subreddit> subreddits) {

        throw new UnsupportedOperationException("Caching is handled by the data tier.");
    }

    @Override
    public void subscribeToSubreddit(@NonNull Subreddit subreddit) {

    }

    @Override
    public void refreshSubreddits() {

    }

    @Override
    public void deleteAllSubreddits() {

    }
}
