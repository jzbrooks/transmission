package com.jzbrooks.transmission.data.source.remote.api;

/**
 * Interface to pass messages on completion from asynctask
 */
public interface FetchRedditDataCompleteListener<T> {
    void onCompleted(T results);
}
