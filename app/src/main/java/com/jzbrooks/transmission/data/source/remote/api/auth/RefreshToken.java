package com.jzbrooks.transmission.data.source.remote.api.auth;

/**
 * A reddit account's refresh token
 */
public final class RefreshToken extends Token {

    public RefreshToken(String token) {
        this.token = token;
    }
}
