package com.jzbrooks.transmission.data.source.remote.api.responses;

/**
 * A reddit account information response
 *
 * /api/v1/me
 */
public final class AccountResponse {
    public String id;
    public String name;
    public boolean over_18;
    public boolean has_mail;
    public int inbox_count;
}