package com.jzbrooks.transmission.data.source.remote.api.auth;

/**
 * Reddit account identity and credentials
 */
public class Account {
    private static final String LOG_TAG = Account.class.getSimpleName();

    public AccessToken accessToken;
    public RefreshToken refreshToken;
    public String username;

    public Account(String username, String refreshToken) {
        this.username = username;
        this.refreshToken = new RefreshToken(refreshToken);
        this.accessToken = new AccessToken();
    }

    public Account(String username, String refreshToken, String accessToken) {
        this.username = username;
        this.refreshToken = new RefreshToken(refreshToken);
        this.accessToken = new AccessToken();
        this.accessToken.token = accessToken;
    }
}
