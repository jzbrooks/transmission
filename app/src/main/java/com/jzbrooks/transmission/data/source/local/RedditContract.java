package com.jzbrooks.transmission.data.source.local;

import android.provider.BaseColumns;

/**
 * Database contract for data pulled from the reddit api
 */
public final class RedditContract {

    /* Inner class that defines the table contents of the posts table */
    public static final class PostEntry implements BaseColumns {

        public static final String TABLE_NAME       = "posts";

        // reddit api post id
        public static final String COLUMN_API_ID    = "api_id";

        // reddit post title
        public static final String COLUMN_TITLE     = "title";

        // reddit post author
        public static final String COLUMN_AUTHOR    = "author";

        // reddit post link
        public static final String COLUMN_URL = "url";

        // reddit subreddit name
        public static final String COLUMN_SUBREDDIT_NAME = "subreddit_name";

        // reddit post self text
        public static final String COLUMN_SELFTEXT = "selftext";

        // reddit post self text
        public static final String COLUMN_SELFTEXT_HTML = "selftext_html";

        // reddit post creation date
        public static final String COLUMN_CREATED = "created";

        // reddit post domain
        public static final String COLUMN_DOMAIN    = "domain";

        // reddit post score
        public static final String COLUMN_SCORE     = "score";

        // reddit nsfw flag (over_18)
        public static final String COLUMN_NSFW      = "nsfw";

        // reddit post comment count
        public static final String COLUMN_COMMENT_COUNT = "comment_count";

        // reddit post visited flag
        public static final String COLUMN_VISITED   = "visited";
    }

    /* Inner class that defines the table contents of the subreddits table */
    public static final class SubredditEntry implements BaseColumns {

        public static final String TABLE_NAME = "subreddits";

        // reddit api subreddit id
        public static final String COLUMN_API_ID               = "api_id";

        // reddit subreddit name
        public static final String COLUMN_SUBREDDIT_NAME       = "subbreddit_name";
    }

    /* Inner class that defines the table contents of the frontpage table */
    public static final class FrontpageEntry implements BaseColumns {

        public static final String TABLE_NAME = "frontpage";

        // foreign key for reddit posts on the front page
        public static final String COLUMN_API_ID = "api_id";
    }

    /* Inner class that defines the table contents of the messages table */
    public static final class MessagesEntry implements BaseColumns {

        public static final String TABLE_NAME = "messages";

        // reddit api id
        public static final String COLUMN_API_ID = "api_id";

        // reddit user (sender)
        public static final String COLUMN_SENDER = "sender";

        // reddit user (reciever)
        public static final String COLUMN_RECIPIENT = "recipient";

        // reddit message body
        public static final String COLUMN_BODY = "body";

        // reddit message date sent
        public static final String COLUMN_DATE = "date";
    }

    /* Inner class that defines the table contents of the users table */
    public static final class UserEntry implements BaseColumns {

        public static final String TABLE_NAME = "users";

        // reddit username
        public static final String COLUMN_USERNAME = "username";

        // reddit api refresh token
        public static final String COLUMN_REFRESH_TOKEN = "refresh_token";
    }
}
