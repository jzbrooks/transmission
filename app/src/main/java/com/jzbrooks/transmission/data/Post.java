package com.jzbrooks.transmission.data;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Model of a reddit post
 */
public class Post {

    private String id;
    private String title;
    private String subreddit;
    private String author;
    private String domain;
    private String url;
    private String selftext;
    private String selftextHtml;
    private String after;
    private long created;
    private long edited;
    private long commentCount;
    private long score;
    private boolean isVisited;
    private boolean isOverEighteen;

    private Post(Builder builder) {
        id = builder.id;
        title = builder.title;
        subreddit = builder.subreddit;
        author = builder.author;
        domain = builder.domain;
        url = builder.url;
        selftext = builder.selftext;
        selftextHtml = builder.selftextHtml;
        after = builder.after;
        created = builder.created;
        edited = builder.edited;
        commentCount = builder.commentCount;
        score = builder.score;
        isVisited = builder.visited;
        isOverEighteen = builder.overEighteen;
    }

    public static class Builder {

        private String id;
        private String title;
        private String subreddit;
        private String author;
        private String domain;
        private String url;
        private String selftext;
        private String selftextHtml;
        private String after;
        private long created;
        private long edited;
        private long score;
        private long commentCount;
        private boolean visited;
        private boolean overEighteen;

        public Builder() {
        }

        public Post build() {

            return new Post(this);
        }

        public Builder id(@NonNull String id) {
            this.id = id;
            return this;
        }

        public Builder title(@NonNull String title) {
            this.title = title;
            return this;
        }

        public Builder subreddit(@NonNull String subreddit) {
            this.subreddit = subreddit;
            return this;
        }

        public Builder author(@NonNull String author) {
            this.author = author;
            return this;
        }

        public Builder domain(@NonNull String domain) {
            this.domain = domain;
            return this;
        }

        public Builder url(@Nullable String url) {
            this.url = url;
            return this;
        }

        public Builder selftext(@Nullable String selftext) {
            this.selftext = selftext;
            return this;
        }

        public Builder selftextHtml(@Nullable String selftextHtml) {
            this.selftextHtml = selftextHtml;
            return this;
        }

        public Builder after(@Nullable String after) {
            this.after = after;
            return this;
        }

        public Builder commentCount(long commentCount) {
            this.commentCount = commentCount;
            return this;
        }

        public Builder created(long created) {
            this.created = created;
            return this;
        }

        public Builder edited(long edited) {
            this.edited = edited;
            return this;
        }

        public Builder score(long score) {
            this.score = score;
            return this;
        }

        public Builder visited(boolean visited) {
            this.visited = visited;
            return this;
        }

        public Builder overEighteen(boolean overEighteen) {
            this.overEighteen = overEighteen;
            return this;
        }
    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    @NonNull
    public String getSubreddit() {
        return subreddit;
    }

    @NonNull
    public String getAuthor() {
        return author;
    }

    @NonNull
    public String getDomain() { return domain; }

    @Nullable
    public String getUrl() {
        return url;
    }

    @Nullable
    public String getSelftext() {
        return selftext;
    }

    @Nullable
    public String getSelftextHtml() {
        return selftextHtml;
    }

    public String getAfter() { return after; }

    public long getCreated() {
        return created;
    }

    public long getEdited() { return edited; }

    public long getCommentCount() { return commentCount; }

    public long getScore() {
        return score;
    }

    public boolean isVisited() { return isVisited; }

    public boolean isOverEighteen() { return isOverEighteen; }
}
