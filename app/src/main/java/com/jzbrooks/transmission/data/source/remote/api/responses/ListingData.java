package com.jzbrooks.transmission.data.source.remote.api.responses;

import java.util.List;

/**
 * Listing data from the reddit api
 */
public final class ListingData {
    public String modhash;
    public List<ListingChild> children;
    public String after;
    public String before;
}
