package com.jzbrooks.transmission.data.source.remote.api.responses;

/**
 * Post response from the reddit api
 */
public final class ListingChild {
    public String kind;
    public ChildData data;
}
