package com.jzbrooks.transmission.data.source.remote.api.auth;

/**
 * A reddit account's OAuth access token
 */
public final class AccessToken extends Token {
    private static final String LOG_TAG = AccessToken.class.getSimpleName();

    private long date = 0;

    public AccessToken() {
        date = System.currentTimeMillis();
    }

    public boolean isTokenExpired() {
        return date > date + 1800000;
    }

}
