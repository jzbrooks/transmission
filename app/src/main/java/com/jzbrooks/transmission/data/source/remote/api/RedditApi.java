package com.jzbrooks.transmission.data.source.remote.api;

import com.jzbrooks.transmission.data.source.remote.api.responses.AccountResponse;
import com.jzbrooks.transmission.data.source.remote.api.responses.ListingResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Retrofit interface for the reddit api
 */
public interface RedditApi {
    String BASE_URL = "https://oauth.reddit.com/";

    @GET("/api/v1/me")
    Call<AccountResponse> getIdentity();

    @GET("/")
    Call<ListingResponse> getPosts(@Query("after") String after);

    @GET("/r/{subreddit_name}")
    Call<ListingResponse> getPostsBySubreddit(@Path("subreddit_name") String subreddit,
                                              @Query("after") String after);

    @GET("/subreddits/mine")
    Call<ListingResponse> getSubreddits();

    @GET("/subreddits.json")
    Call<ListingResponse> getDefaultSubreddits();
}
