package com.jzbrooks.transmission.data.source.remote.api.responses;

/**
 * Response from refreshing auth_tokens against
 * https://www.reddit.com/api/v1/access_token
 */
public final class RefreshAuthResponse {

    public String access_token;
    public String token_type;
    public long expires_in;
    public String scope;

}
