package com.jzbrooks.transmission.data;

import android.support.annotation.NonNull;

/**
 * Model of a subreddit
 */
public final class Subreddit {
    private String id;
    private String name;

    private Subreddit(Builder builder) {
        id = builder.id;
        name = builder.name;
    }

    public static class Builder {

        private String id;
        private String name;

        public Builder() { }

        public Subreddit build() {

            return new Subreddit(this);
        }

        public Builder id(@NonNull String id) {
            this.id = id;
            return this;
        }

        public Builder name(@NonNull String name) {
            this.name = name;
            return this;
        }
    }

    @NonNull
    public String getId() { return id; }

    @NonNull
    public String getName() { return name; }
}
