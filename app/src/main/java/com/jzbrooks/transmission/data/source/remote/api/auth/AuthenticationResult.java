package com.jzbrooks.transmission.data.source.remote.api.auth;

import android.util.Log;

import com.jzbrooks.transmission.data.User;

/**
 * Wraps a user for success and error message for failure.
 */
final class AuthenticationResult {
    private static final String LOG_TAG = AuthenticationResult.class.getSimpleName();

    private User user;
    private String errorMessage;

    User getUser() {
        return user;
    }

    void setUser(User user) {
        this.user = user;
    }

    String getErrorMessage() {
        return errorMessage;
    }

    void setErrorMessage(String errorMessage) {
        Log.e(LOG_TAG, errorMessage);
        this.errorMessage = errorMessage;
    }
}
