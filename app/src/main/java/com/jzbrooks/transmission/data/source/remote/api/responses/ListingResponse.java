package com.jzbrooks.transmission.data.source.remote.api.responses;

/**
 * Listing response from the reddit api
 */
public final class ListingResponse {
    public String kind;
    public ListingData data;
}
