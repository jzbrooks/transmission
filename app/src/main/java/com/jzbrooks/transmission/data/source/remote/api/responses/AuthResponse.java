package com.jzbrooks.transmission.data.source.remote.api.responses;

/**
 * Retrofit authentication response
 */
public final class AuthResponse {

    public String access_token;
    public String token_type;
    public long expires_in;
    public String scope;
    public String refresh_token;

}
