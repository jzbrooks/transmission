package com.jzbrooks.transmission.data.source.remote.api.auth;

import android.os.AsyncTask;
import android.util.Log;

import com.jzbrooks.transmission.data.User;
import com.jzbrooks.transmission.data.source.RedditDataSource;
import com.jzbrooks.transmission.data.source.remote.ApiUtilities;
import com.jzbrooks.transmission.data.source.remote.api.RedditAuthApi;
import com.jzbrooks.transmission.data.source.remote.api.responses.RefreshAuthResponse;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

/**
 * Created by jzbrooks on 4/20/2017.
 */
public final class RefreshAccessTokenTask extends AsyncTask<Object, Void, AuthenticationResult> {
    private static final String LOG_TAG = RefreshAccessTokenTask.class.getSimpleName();
    private RedditDataSource.LoginUserCallback callback;

    @Override
    protected AuthenticationResult doInBackground(Object... params) {
        if (params == null || !(params[0] instanceof User)) {
            Log.d(LOG_TAG, "Async fetch access token task needs refresh token");
        }
        User user = (User)params[0];

        AuthenticationResult result = new AuthenticationResult();

        try {
            Retrofit authRetrofit = new Retrofit.Builder()
                    .baseUrl(RedditAuthApi.AUTH_BASE_URL)
                    .client(ApiUtilities.Companion.getRefreshClient(user.getRefreshToken()))
                    .addConverterFactory(MoshiConverterFactory.create())
                    .build();

            RedditAuthApi authApi = authRetrofit.create(RedditAuthApi.class);
            Call<RefreshAuthResponse> refreshResponseCall = authApi.getRefreshedAccessToken();
            Response<RefreshAuthResponse> retroResponse = refreshResponseCall.execute();

            if (retroResponse.code() != 200) {
                result.setErrorMessage(
                        String.format("There was a problem refreshing " +
                                      "the access token. HTTP code: %s", retroResponse.code()));
            } else {
                RefreshAuthResponse refreshAuthResponse = retroResponse.body();
                user.setAccessToken(refreshAuthResponse.access_token);
                result.setUser(user);
            }
        } catch (IOException ex) {
            result.setErrorMessage("HTTP IO error");
            Log.d(LOG_TAG, "HTTP IO error");
        }

        return result;
    }

    @Override
    protected void onPostExecute(AuthenticationResult authResult) {
        User user = authResult.getUser();

        if (user != null) {
            callback.onLogin(user);
        } else {
            callback.onFailure(authResult.getErrorMessage());
        }
    }

    public void setCallback(RedditDataSource.LoginUserCallback callback) {
        this.callback = callback;
    }
}