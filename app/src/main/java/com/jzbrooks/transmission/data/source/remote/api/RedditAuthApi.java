package com.jzbrooks.transmission.data.source.remote.api;

import android.net.Uri;

import com.jzbrooks.transmission.data.source.remote.api.responses.AuthResponse;
import com.jzbrooks.transmission.data.source.remote.api.responses.RefreshAuthResponse;

import retrofit2.Call;
import retrofit2.http.POST;

/**
 * Reddit Authentication API
 */
public interface RedditAuthApi {
    String AUTH_BASE_URL = "https://api.reddit.com/";
    String REDIRECT_URL = "http://www.transmissionforreddit.com/auth_redirect";
    String CLIENT_ID = "ZBdO3pppMt0cPg";
    String ALL_SCOPES
            = "identity,edit,flair,history,modconfig,modflair,modlog,modposts,modwiki,mysubreddits,privatemessages,"
            + "read,report,save,submit,subscribe,vote,wikiedit,wikiread";

    String PROMPT_URI = Uri.parse("https://www.reddit.com/api/v1/authorize.compact").buildUpon()
                    .appendQueryParameter("response_type", "code")
                    .appendQueryParameter("duration", "permanent")
                    .appendQueryParameter("state", "random_string_of_my_choosing")
                    .appendQueryParameter("redirect_uri", REDIRECT_URL)
                    .appendQueryParameter("client_id", CLIENT_ID)
                    .appendQueryParameter("scope", ALL_SCOPES)
                    .build().toString();

    @POST("/api/v1/access_token")
    Call<AuthResponse> getAccessToken();

    @POST("/api/v1/access_token")
    Call<RefreshAuthResponse> getRefreshedAccessToken();
}
