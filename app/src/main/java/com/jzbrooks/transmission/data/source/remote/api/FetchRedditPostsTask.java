package com.jzbrooks.transmission.data.source.remote.api;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import com.jzbrooks.transmission.data.Post;
import com.jzbrooks.transmission.data.source.RedditDataSource;
import com.jzbrooks.transmission.data.source.remote.ApiUtilities;
import com.jzbrooks.transmission.data.source.remote.api.responses.ListingData;
import com.jzbrooks.transmission.data.source.remote.api.responses.ListingResponse;
import com.jzbrooks.transmission.data.source.remote.api.responses.ListingChild;

/**
 * Async task for pulling reddit api data
 */
public final class FetchRedditPostsTask extends AsyncTask<String, Void, List<Post>> {

    private static final String LOG_TAG = FetchRedditPostsTask.class.getSimpleName();

    private RedditDataSource.LoadPostsCallback callback;

    @Override
    protected List<Post> doInBackground(String... params) {
        List<Post> posts = new ArrayList<>(25);
        String lastLoadedId = "";
        String currentSubreddit = null;

        try {
            Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
                    .addConverterFactory(MoshiConverterFactory.create());

            if (params.length > 0 && params[0] != null) {
                retrofitBuilder.baseUrl(RedditApi.BASE_URL);
                retrofitBuilder.client(ApiUtilities.Companion.getAuthenticatedClient(params[0]));
            } else {
                retrofitBuilder.baseUrl("https://api.reddit.com");
            }

            if (params.length > 1 && params[1] != null)
            {
                currentSubreddit = params[1];
            }

            if (params.length > 2 && params[2] != null) {
                lastLoadedId = params[2];
            }

            Retrofit retrofit = retrofitBuilder.build();
            RedditApi api = retrofit.create(RedditApi.class);
            Response<ListingResponse> apiCall = null;

            if (currentSubreddit == null) {
                apiCall = api.getPosts(lastLoadedId).execute();
            } else {
                apiCall = api.getPostsBySubreddit(currentSubreddit, lastLoadedId).execute();
            }

            ListingResponse listing = apiCall.body();
            ListingData data = listing.data;

            for (int i=0; i<data.children.size(); i++) {
                ListingChild child = data.children.get(i);

                Post post = new Post.Builder()
                        .id(child.data.id)
                        .title(child.data.title)
                        .subreddit(child.data.subreddit)
                        .author(child.data.author)
                        .domain(child.data.domain)
                        .url(child.data.url)
                        .selftext(child.data.selftext)
                        .selftextHtml(child.data.selftext_html)
                        .after(data.after)
                        .created(child.data.created)
                        .commentCount(child.data.num_comments)
                        .score(child.data.score)
                        .visited(child.data.visited)
                        .overEighteen(child.data.over_18)
                        .build();

                posts.add(post);
            }

            Log.v(LOG_TAG, String.format("Found %d posts.", posts.size()));
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error ", e);
        }

        return posts;
    }

    @Override
    public void onPostExecute(List<Post> posts) {
        if (!posts.isEmpty()) {
            this.callback.onPostsLoaded(posts);
        }
        else {
            Log.e(LOG_TAG, "No listener for completion");
            this.callback.onDataNotAvailable();
        }
    }

    public void setCallback(RedditDataSource.LoadPostsCallback callback) {
        this.callback = callback;
    }
}
