package com.jzbrooks.transmission.data.source.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Database helper for reddit data downloaded from the reddit api
 */
public final class RedditDatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 9;

    private static final String DATABASE_NAME = "transmission.db";

    public RedditDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String SQL_CREATE_POST_TABLE = "CREATE TABLE " + RedditContract.PostEntry.TABLE_NAME + " ("
                + RedditContract.PostEntry._ID + " INTEGER PRIMARY KEY, "
                + RedditContract.PostEntry.COLUMN_API_ID + " TEXT NOT NULL, "
                + RedditContract.PostEntry.COLUMN_TITLE + " TEXT NOT NULL, "
                + RedditContract.PostEntry.COLUMN_AUTHOR + " TEXT NOT NULL, "
                + RedditContract.PostEntry.COLUMN_URL + " TEXT NOT NULL, "
                + RedditContract.PostEntry.COLUMN_SUBREDDIT_NAME + " TEXT NOT NULL, "
                + RedditContract.PostEntry.COLUMN_SELFTEXT + " TEXT, "
                + RedditContract.PostEntry.COLUMN_SELFTEXT_HTML + " TEXT, "
                + RedditContract.PostEntry.COLUMN_CREATED + " INTEGER NOT NULL, "
                + RedditContract.PostEntry.COLUMN_DOMAIN + " TEXT NOT NULL, "
                + RedditContract.PostEntry.COLUMN_SCORE + " INTEGER NOT NULL, "
                + RedditContract.PostEntry.COLUMN_COMMENT_COUNT + " INTEGER NOT NULL, "
                + RedditContract.PostEntry.COLUMN_NSFW + " BOOLEAN NOT NULL, "
                + RedditContract.PostEntry.COLUMN_VISITED + " BOOLEAN NOT NULL, "
                + " UNIQUE (" + RedditContract.PostEntry.COLUMN_API_ID + ") ON CONFLICT REPLACE);";

        final String SQL_CREATE_SUBREDDIT_TABLE = "CREATE TABLE " + RedditContract.SubredditEntry.TABLE_NAME + " ("
                + RedditContract.SubredditEntry._ID + " INTEGER PRIMARY KEY, "
                + RedditContract.SubredditEntry.COLUMN_API_ID + " TEXT NOT NULL, "
                + RedditContract.SubredditEntry.COLUMN_SUBREDDIT_NAME + " TEXT NOT NULL, "
                + " UNIQUE (" + RedditContract.SubredditEntry.COLUMN_API_ID + ") ON CONFLICT REPLACE);";

        final String SQL_CREATE_FRONTPAGE_TABLE = "CREATE TABLE " + RedditContract.FrontpageEntry.TABLE_NAME + " ("
                + RedditContract.FrontpageEntry._ID + " INTEGER PRIMARY KEY, "
                + RedditContract.FrontpageEntry.COLUMN_API_ID + " TEXT NOT NULL, "
                + "FOREIGN KEY (" + RedditContract.FrontpageEntry.COLUMN_API_ID + ") REFERENCES "
                + RedditContract.PostEntry.TABLE_NAME + " (" + RedditContract.PostEntry.COLUMN_API_ID + "));";

        final String SQL_CREATE_MESSAGES_TABLE = "CREATE TABLE " + RedditContract.MessagesEntry.TABLE_NAME + " ("
                + RedditContract.MessagesEntry._ID + " INTEGER PRIMARY KEY, "
                + RedditContract.MessagesEntry.COLUMN_API_ID + " TEXT NOT NULL, "
                + RedditContract.MessagesEntry.COLUMN_SENDER + " TEXT NOT NULL, "
                + RedditContract.MessagesEntry.COLUMN_RECIPIENT + " TEXT NOT NULL, "
                + RedditContract.MessagesEntry.COLUMN_BODY + " TEXT NOT NULL, "
                + RedditContract.MessagesEntry.COLUMN_DATE + " INTEGER NOT NULL, "
                + " UNIQUE (" + RedditContract.MessagesEntry.COLUMN_API_ID + ") ON CONFLICT REPLACE);";

        final String SQL_CREATE_USERS_TABLE = "CREATE TABLE " + RedditContract.UserEntry.TABLE_NAME + " ("
                + RedditContract.UserEntry._ID + " INTEGER PRIMARY KEY, "
                + RedditContract.UserEntry.COLUMN_USERNAME + " TEXT NOT NULL, "
                + RedditContract.UserEntry.COLUMN_REFRESH_TOKEN + " TEXT NOT NULL, "
                + " UNIQUE (" + RedditContract.UserEntry.COLUMN_USERNAME + ") ON CONFLICT REPLACE);";

        sqLiteDatabase.execSQL(SQL_CREATE_POST_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_SUBREDDIT_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_FRONTPAGE_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_MESSAGES_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_USERS_TABLE);
    }

    @Override   // since this is a cache, we just delete everything and start over
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + RedditContract.PostEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + RedditContract.SubredditEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + RedditContract.FrontpageEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + RedditContract.MessagesEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + RedditContract.UserEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
