package com.jzbrooks.transmission.data.source.remote.api;

import android.os.AsyncTask;
import android.util.Log;

import com.jzbrooks.transmission.data.Subreddit;
import com.jzbrooks.transmission.data.source.RedditDataSource;
import com.jzbrooks.transmission.data.source.remote.ApiUtilities;
import com.jzbrooks.transmission.data.source.remote.api.responses.ListingChild;
import com.jzbrooks.transmission.data.source.remote.api.responses.ListingData;
import com.jzbrooks.transmission.data.source.remote.api.responses.ListingResponse;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

/**
 * Fetches subreddits for the logged in user
 */
public final class FetchSubredditsTask extends AsyncTask<String, Void, List<Subreddit>> {

    private static final String LOG_TAG = FetchSubredditsTask.class.getSimpleName();

    private RedditDataSource.LoadSubredditsCallback callback;

    @Override
    protected List<Subreddit> doInBackground(String... params) {
        if (params.length < 1) throw new InvalidParameterException();

        List<Subreddit> subreddits = new ArrayList<>();

        Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
                .addConverterFactory(MoshiConverterFactory.create());

        boolean authenticated = false;

        if (params[0] != null) {
            authenticated = true;
            retrofitBuilder.baseUrl(RedditApi.BASE_URL);
            retrofitBuilder.client(ApiUtilities.Companion.getAuthenticatedClient(params[0]));
        } else {
            retrofitBuilder.baseUrl("https://api.reddit.com");
        }

        Retrofit retrofit = retrofitBuilder.build();

        try {

            RedditApi api = retrofit.create(RedditApi.class);
            Call<ListingResponse> call = authenticated ? api.getSubreddits() : api.getDefaultSubreddits();
            Response<ListingResponse> response = call.execute();
            ListingResponse responseBody = response.body();
            ListingData data = responseBody.data;

            for (int i = 0; i < data.children.size(); i++) {
                ListingChild subredditData = data.children.get(i);

                Subreddit subreddit = new Subreddit.Builder()
                        .id(subredditData.data.id)
                        .name(subredditData.data.display_name)
                        .build();

                subreddits.add(subreddit);
            }

        } catch (IOException ex) {

            Log.e(LOG_TAG, "Failed to get subreddits from reddit api", ex);
        }

        return subreddits;
    }

    @Override
    public void onPostExecute(List<Subreddit> subreddits) {

        this.callback.onSubredditsLoaded(subreddits);
    }

    public void setCallback(RedditDataSource.LoadSubredditsCallback callback) {

        this.callback = callback;
    }
}
