package com.jzbrooks.transmission.utility;

import android.content.Context;
import android.support.annotation.NonNull;

import com.jzbrooks.transmission.data.source.RedditDataStore;
import com.jzbrooks.transmission.data.source.local.RedditLocalDataSource;
import com.jzbrooks.transmission.data.source.remote.RedditRemoteDataSource;

/**
 * Simple dependency injection
 */
public final class Injection {

    @NonNull
    public static RedditDataStore provideRedditRepo(@NonNull Context context) {
        return RedditDataStore.getInstance(
                RedditRemoteDataSource.getInstance(),
                RedditLocalDataSource.getInstance(context)
        );
    }
}
