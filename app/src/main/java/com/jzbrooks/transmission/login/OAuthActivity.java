package com.jzbrooks.transmission.login;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.jzbrooks.transmission.utility.Injection;

public class OAuthActivity extends AppCompatActivity implements OAuthContract.View {

    private OAuthContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new OAuthPresenter(Injection.provideRedditRepo(this), this);
    }

    @Override
    public void onResume() {
        super.onResume();

        presenter.start();
    }

    @Override
    public void setPresenter(OAuthContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showLoginScreen(@NonNull String initialUrl) {
        WebView webView = new WebView(this);
        webView.clearCache(false);
        webView.clearHistory();
        webView.loadUrl(initialUrl);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return presenter.grantAccess(OAuthActivity.this.getApplicationContext(), url);
            }
        });

        this.setContentView(webView);
    }

    @Override
    public void hideLoginScreen() {
        this.finish();
    }
}