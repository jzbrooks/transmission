package com.jzbrooks.transmission.login;

import android.content.Context;
import android.support.annotation.NonNull;

import com.jzbrooks.transmission.BasePresenter;
import com.jzbrooks.transmission.BaseView;

/**
 * Contract for the login flow OAuth view and model
 */
public interface OAuthContract {

    interface Presenter extends BasePresenter {

        boolean grantAccess(Context context, String redirectUrl);
    }

    interface View extends BaseView<Presenter> {

        void showLoginScreen(@NonNull String url);

        void hideLoginScreen();
    }
}
