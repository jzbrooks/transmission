package com.jzbrooks.transmission.login;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.jzbrooks.transmission.R;
import com.jzbrooks.transmission.data.User;
import com.jzbrooks.transmission.data.source.RedditDataSource;
import com.jzbrooks.transmission.data.source.RedditDataStore;
import com.jzbrooks.transmission.data.source.remote.api.RedditAuthApi;

/**
 * OAuth login provider for the Reddit OAuth Flow
 */
public class OAuthPresenter implements OAuthContract.Presenter {

    private final OAuthContract.View loginView;
    private final RedditDataStore dataStore;

    public OAuthPresenter(@NonNull RedditDataStore dataStore, @NonNull OAuthContract.View loginView) {
        this.loginView = loginView;
        this.dataStore = dataStore;

        loginView.setPresenter(this);
    }

    @Override
    public void start() {
        loginView.showLoginScreen(RedditAuthApi.PROMPT_URI);
    }

    @Override
    public boolean grantAccess(final Context context, String redirectUrl) {
        if (redirectUrl.startsWith(RedditAuthApi.REDIRECT_URL)) {
            dataStore.signIn(redirectUrl, new RedditDataSource.LoginUserCallback() {
                @Override
                public void onLogin(User user) {

                    dataStore.storeUser(user);
                    dataStore.setCurrentUser(user);

                    loginView.hideLoginScreen();

                    String prefsFile = context.getString(R.string.shared_preferences_file);
                    String signedInOutKey = context.getString(R.string.pref_key_signed_in);

                    SharedPreferences preferences =
                            context.getSharedPreferences(prefsFile, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean(signedInOutKey, true);
                    editor.apply();
                }

                @Override
                public void onFailure(String errorMessage) {

                    loginView.hideLoginScreen();
                }
            });

            return true;
        }

        return false;
    }
}
