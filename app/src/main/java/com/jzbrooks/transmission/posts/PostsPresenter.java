package com.jzbrooks.transmission.posts;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Menu;

import com.jzbrooks.transmission.R;
import com.jzbrooks.transmission.data.Post;
import com.jzbrooks.transmission.data.User;
import com.jzbrooks.transmission.data.source.RedditDataSource;
import com.jzbrooks.transmission.data.source.RedditDataStore;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Post presenter for reddit posts
 */
public class PostsPresenter implements PostsContract.Presenter {

    private RedditDataStore dataStore;
    private PostsContract.View view;

    private String lastLoadedListingId = "";
    private String subredditSource;

    public PostsPresenter(@NonNull RedditDataStore store, @NonNull PostsContract.View view) {
        this.dataStore = store;
        this.view = view;

        this.view.setPresenter(this);
    }

    @Override
    public void start() {

        loadPosts();
    }

    @Override
    public void loadPosts() {

        view.setLoadingIndicator(true);

        dataStore.getCurrentUser(new RedditDataSource.FetchUserCallback() {
            @Override
            public void onSuccess(User user) {

                getPosts(user);
            }

            @Override
            public void onFailure() {

                getPosts(null);
            }
        });
    }

    @Override
    public void loadMorePosts() {

        view.setLoadingIndicator(true);

        dataStore.getCurrentUser(new RedditDataSource.FetchUserCallback() {
            @Override
            public void onSuccess(User user) {

                getMorePosts(user, lastLoadedListingId);
            }

            @Override
            public void onFailure() {

                getMorePosts(null, lastLoadedListingId);
            }
        });
    }

    @Override
    public void setCurrentSubredditSource(@Nullable String subredditSource) {
        this.subredditSource = subredditSource;
    }

    private void getPosts(@Nullable User currentUser) {

        final String beginningListingId = "";

        dataStore.getPosts(currentUser, subredditSource, beginningListingId,
                new RedditDataSource.LoadPostsCallback() {

                @Override
                public void onPostsLoaded(List<Post> posts) {
                    view.setLoadingIndicator(false);

                    view.showPosts(posts);

                    lastLoadedListingId = posts.get(posts.size()-1).getAfter();
                }

                @Override
                public void onDataNotAvailable() {
                    view.setLoadingIndicator(false);
                }
        });
    }

    private void getMorePosts(@Nullable User currentUser, @NonNull String lastLoadedId) {

        dataStore.getPosts(currentUser, subredditSource, lastLoadedId,
                new RedditDataSource.LoadPostsCallback() {

                @Override
                public void onPostsLoaded(List<Post> posts) {
                    view.setLoadingIndicator(false);

                    view.showMorePosts(posts);

                    lastLoadedListingId = posts.get(posts.size()-1).getAfter();
                }

                @Override
                public void onDataNotAvailable() {
                    view.setLoadingIndicator(false);
                }
        });
    }

    @Override
    public void setupMenu(Context context, Menu menu) {
        final int SignInOutItemIndex = 1;

        String prefsFile = context.getString(R.string.shared_preferences_file);

        SharedPreferences preferences =
                context.getSharedPreferences(prefsFile, MODE_PRIVATE);

        String signedInKey = context.getString(R.string.pref_key_signed_in);
        boolean signedIn = preferences.getBoolean(signedInKey, false);
        String title = signedIn ? context.getString(R.string.menu_label_signout)
                : context.getString(R.string.menu_label_signin);

        menu.getItem(SignInOutItemIndex).setTitle(title);
    }
}
