package com.jzbrooks.transmission.posts;

import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jzbrooks.transmission.R;
import com.jzbrooks.transmission.data.Post;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * RecyclerView adapter for reddit post items in the main feed
 */
public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.PostItemHolder> {

    private ArrayList<Post> redditPosts;

    PostsAdapter() {
        redditPosts = new ArrayList<>(25);
    }

    @Override
    public PostItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_feed_item, parent, false);
        return new PostItemHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(PostItemHolder holder, int position) {

        Post post = redditPosts.get(position);
        holder.bindFeedItem(post);
    }

    @Override
    public int getItemCount() {

        return redditPosts.size();
    }

    void add(Post post) {

        redditPosts.add(post);
        notifyItemInserted(redditPosts.size()-1);
    }

    void replaceData(List<Post> posts) {

        clear();
        addRange(posts);
    }

    void addRange(List<Post> posts) {

        final int insertedIndex = redditPosts.size() + 1;
        redditPosts.addAll(posts);
        notifyItemRangeInserted(insertedIndex, posts.size());
    }

    private void clear() {

        int size = redditPosts.size();
        redditPosts.clear();
        notifyItemRangeRemoved(0, size);
    }

    class PostItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView titleView;
        private TextView dateView;

        private String url;

        private Calendar calendar;
        private TimeZone timeZone;
        private SimpleDateFormat formatter;

        PostItemHolder(View itemView) {

            super(itemView);
            titleView = (TextView) itemView.findViewById(R.id.item_title);
            dateView = (TextView) itemView.findViewById(R.id.item_date);

            calendar = Calendar.getInstance();
            timeZone = calendar.getTimeZone();
            formatter = new SimpleDateFormat("MM.dd.yyyy", Locale.US);
            formatter.setTimeZone(timeZone);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            Log.d("LIST ITEM CLICK", titleView.getText() + " - " + dateView.getText());

            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            int color = ContextCompat.getColor(view.getContext(), R.color.colorPrimary);
            CustomTabsIntent customTabsIntent = builder
                    .setToolbarColor(color)
                    .addDefaultShareMenuItem()
                    .build();

            customTabsIntent.launchUrl(view.getContext(), Uri.parse(url));
        }

        void bindFeedItem(Post post) {
            titleView.setText(post.getTitle());
            dateView.setText(formatter.format(new Date(post.getCreated() * 1000)));
            url = post.getUrl();
        }
    }
}
