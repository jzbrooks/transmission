package com.jzbrooks.transmission.posts;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.Menu;

import com.jzbrooks.transmission.BasePresenter;
import com.jzbrooks.transmission.BaseView;
import com.jzbrooks.transmission.data.Post;

import java.util.List;

/**
 * Contract for the view and presenter for reddit posts
 */
public interface PostsContract {

    interface View extends BaseView<Presenter> {

        void setLoadingIndicator(boolean active);

        void showPosts(List<Post> posts);

        void showMorePosts(List<Post> posts);
    }

    interface Presenter extends BasePresenter {

        void loadPosts();

        void loadMorePosts();

        void setCurrentSubredditSource(@Nullable String subredditSource);

        void setupMenu(Context context, Menu menu);
    }
}
