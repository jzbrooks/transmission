package com.jzbrooks.transmission.posts;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.jzbrooks.transmission.R;
import com.jzbrooks.transmission.data.Post;
import com.jzbrooks.transmission.login.OAuthActivity;
import com.jzbrooks.transmission.main.MainContract;
import com.jzbrooks.transmission.utility.Injection;

import java.util.List;

/**
 * A fragment containing a feed for reddit posts
 */
public class PostsFragment extends Fragment implements PostsContract.View {

    private PostsAdapter adapter;
    private PostsContract.Presenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        adapter = new PostsAdapter();

        presenter = new PostsPresenter(
                Injection.provideRedditRepo(getActivity()),
                this
        );

        Bundle args = getArguments();
        if (args != null) {
            String selectedSubreddit = args.getString(MainContract.SUBREDDIT_BUNDLE_KEY);
            presenter.setCurrentSubredditSource(selectedSubreddit);
        }
    }

    @Override
    public void onResume() {

        super.onResume();

        presenter.start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_feed, container, false);

        RecyclerView mRecyclerView = (RecyclerView) view.findViewById(R.id.view_feed);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(view.getContext());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.addOnScrollListener(new EndlessScrollListener(mLinearLayoutManager) {
            @Override
            public void onLoadMore(int totalItemsCount) {

                presenter.loadMorePosts();
            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_feed, menu);

        presenter.setupMenu(getContext(), menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_refresh:
                presenter.loadPosts();
                return true;
            case R.id.action_signin:
                Intent signInIntent = new Intent(getActivity(), OAuthActivity.class);
                startActivity(signInIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void setPresenter(PostsContract.Presenter presenter) {

        this.presenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {

        //todo(jzb): implement loading indicator
    }

    @Override
    public void showPosts(List<Post> posts) {

        adapter.replaceData(posts);
    }

    @Override
    public void showMorePosts(List<Post> posts) {

        adapter.addRange(posts);
    }
}
