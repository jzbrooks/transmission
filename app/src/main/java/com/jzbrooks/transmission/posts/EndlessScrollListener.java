package com.jzbrooks.transmission.posts;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Facilitates endless scrolling by coordinating api get requests on scrolling
 */
public abstract class EndlessScrollListener extends RecyclerView.OnScrollListener {

    private RecyclerView.LayoutManager layoutManager;
    private int previousTotalItemCount = 0;
    private boolean isLoading = true;

    EndlessScrollListener(RecyclerView.LayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    @Override
    public void onScrolled(RecyclerView view, int dx, int dy) {
        final int visibleThreshold = 4;

        int totalItemCount = layoutManager.getItemCount();
        int lastVisibleItemPosition =
                ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();

        if (totalItemCount < previousTotalItemCount) {
            this.previousTotalItemCount = totalItemCount;
            if (totalItemCount == 0) {
                this.isLoading = true;
            }
        }

        if (isLoading && (totalItemCount > previousTotalItemCount)) {
            isLoading = false;
            previousTotalItemCount = totalItemCount;
        }

        if (!isLoading && (lastVisibleItemPosition + visibleThreshold) > totalItemCount) {
            onLoadMore(totalItemCount);
            isLoading = true;
        }
    }

    // Defines the process for actually loading more data based on page
    public abstract void onLoadMore(int totalItemsCount);
}
