package com.jzbrooks.transmission.main;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.Menu;

import com.jzbrooks.transmission.BasePresenter;
import com.jzbrooks.transmission.BaseView;
import com.jzbrooks.transmission.data.Subreddit;

import java.util.List;

/**
 * Contract for the view and presenter the main application view
 */
public interface MainContract {

    String SUBREDDIT_BUNDLE_KEY = "subredditName";

    enum MenuItem {
        Posts,
        Messages,
        Subreddits
    }

    interface View extends BaseView<Presenter> {

        void showPostsView(@Nullable String currentSubreddit);

        void showMessagesView();

        void showSubredditsView(List<Subreddit> subreddits);
    }

    interface Presenter extends BasePresenter {

        void getSubreddits();

        void setCurrentSubreddit(@Nullable String subredditName);

        void menuClicked(MenuItem item);
    }
}
