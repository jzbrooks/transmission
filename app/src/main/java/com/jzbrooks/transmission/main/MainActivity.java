package com.jzbrooks.transmission.main;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;

import com.jzbrooks.transmission.R;
import com.jzbrooks.transmission.data.Subreddit;
import com.jzbrooks.transmission.posts.PostsFragment;
import com.jzbrooks.transmission.utility.Injection;

import java.util.List;

public class MainActivity extends AppCompatActivity implements MainContract.View {

    private MainContract.Presenter presenter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final BottomNavigationView navigationView =
                (BottomNavigationView) findViewById(R.id.bottom_navigation);

        navigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                int itemId = item.getItemId();
                switch (itemId) {

                    case R.id.action_nav_feed:
                        presenter.menuClicked(MainContract.MenuItem.Posts);
                        return true;

                    case R.id.action_nav_messages:
                        presenter.menuClicked(MainContract.MenuItem.Messages);
                        return true;

                    case R.id.action_nav_subreddits:
                        presenter.menuClicked(MainContract.MenuItem.Subreddits);
                        return true;

                    default:
                        return false;
                }
            }
        });

        presenter = new MainPresenter(
                Injection.provideRedditRepo(this),
                this
        );

        showPostsView(null);
    }

    @Override
    public void setPresenter(MainContract.Presenter presenter) {

        this.presenter = presenter;
    }

    @Override
    public void showPostsView(@Nullable String selectedSubredditName) {

        PostsFragment postsFragment = new PostsFragment();

        if (selectedSubredditName != null) {
            Bundle bundle = new Bundle();
            bundle.putString(MainContract.SUBREDDIT_BUNDLE_KEY, selectedSubredditName);
            postsFragment.setArguments(bundle);
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, postsFragment)
                .commit();
    }

    @Override
    public void showMessagesView() {

    }

    @Override
    public void showSubredditsView(List<Subreddit> subreddits) {
        PopupMenu menu = new PopupMenu(this, findViewById(R.id.action_nav_subreddits));

        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                String subredditName = item.getTitle().toString();

                if (!subredditName.equals("frontpage")) {
                    presenter.setCurrentSubreddit(subredditName);
                } else {
                    presenter.setCurrentSubreddit(null);
                }

                return false;
            }
        });

        menu.getMenu().add("frontpage");
        menu.getMenu().add("popular");
        menu.getMenu().add("all");

        for (Subreddit subreddit : subreddits) {
            menu.getMenu().add(subreddit.getName());
        }

        menu.show();
    }
}
