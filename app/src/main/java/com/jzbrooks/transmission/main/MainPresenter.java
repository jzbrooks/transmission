package com.jzbrooks.transmission.main;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Menu;

import com.jzbrooks.transmission.R;
import com.jzbrooks.transmission.data.Subreddit;
import com.jzbrooks.transmission.data.User;
import com.jzbrooks.transmission.data.source.RedditDataSource;
import com.jzbrooks.transmission.data.source.RedditDataStore;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Main application presenter
 */
public class MainPresenter implements MainContract.Presenter {

    private static final String LOG_TAG = MainPresenter.class.getSimpleName();

    private MainContract.View view = null;
    private RedditDataStore dataStore = null;
    private String currentSubreddit;

    public MainPresenter(@NonNull RedditDataStore dataStore, @NonNull MainContract.View view) {
        this.view = view;
        this.dataStore = dataStore;

        view.setPresenter(this);
    }

    @Override
    public void start() {

    }

    @Override
    public void getSubreddits() {

        dataStore.getCurrentUser(new RedditDataSource.FetchUserCallback() {
            @Override
            public void onSuccess(User user) {

                getSubreddits(user);
            }

            @Override
            public void onFailure() {

                getSubreddits(null);
            }
        });
    }

    @Override
    public void setCurrentSubreddit(@Nullable String subredditName) {
        this.currentSubreddit = subredditName;

        view.showPostsView(this.currentSubreddit);
    }

    @Override
    public void menuClicked(MainContract.MenuItem item) {

        switch (item) {
            case Posts:
                view.showPostsView(currentSubreddit);
                break;
            case Messages:
                view.showMessagesView();
                break;
            case Subreddits:
                getSubreddits();
                break;
        }
    }

    private void getSubreddits(@Nullable final User user) {

        dataStore.getSubreddits(user, new RedditDataSource.LoadSubredditsCallback() {
            @Override
            public void onSubredditsLoaded(List<Subreddit> subreddits) {

                view.showSubredditsView(subreddits);
            }

            @Override
            public void onDataNotAvailable() {

                String errorMessage;

                if (user == null) {

                    errorMessage = "There was a problem getting subreddits.";
                } else {

                    errorMessage =
                    String.format(
                            "There was a problem getting subreddits for /u/%s .",
                            user.getUsername()
                    );
                }

                Log.e(LOG_TAG, errorMessage);
            }
        });
    }
}
