package com.jzbrooks.transmission;

/**
 * Implemented by all views in the system
 */
public interface BaseView<T> {
    void setPresenter(T presenter);
}
