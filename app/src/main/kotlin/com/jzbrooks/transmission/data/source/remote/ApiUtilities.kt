package com.jzbrooks.transmission.data.source.remote

import android.content.Context
import android.util.Base64
import com.jzbrooks.transmission.data.source.remote.api.RedditAuthApi
import okhttp3.*

/**
 * Utilities for the reddit api
 */
class ApiUtilities {
    companion object {
        fun getAuthenticatedClient(accessToken: String): OkHttpClient {
            return OkHttpClient().newBuilder().addInterceptor({chain ->
                val originalRequest = chain.request()
                val builder = originalRequest.newBuilder()
                        .header("Authorization" , String.format("bearer %s", accessToken))
                val newRequest = builder.build()
                chain.proceed(newRequest)
            }).build()
        }

        fun getSignInClient(code: String): OkHttpClient {
            return OkHttpClient().newBuilder().addInterceptor({ chain ->
                val originalRequest = chain.request()

                val formBody = FormBody.Builder()
                        .add("grant_type", "authorization_code")
                        .add("code", code)
                        .add("redirect_uri", RedditAuthApi.REDIRECT_URL)
                        .build()

                val builder = originalRequest.newBuilder()
                        .addHeader("Authorization", "Basic " +
                                Base64.encodeToString((RedditAuthApi.CLIENT_ID + ":").toByteArray(),
                                Base64.URL_SAFE or Base64.NO_WRAP))
                        .post(formBody)

                val newRequest = builder.build()
                chain.proceed(newRequest)
            }).build()
        }

        fun getRefreshClient(refreshToken: String): OkHttpClient {
            return OkHttpClient().newBuilder().addInterceptor({ chain ->
                val originalRequest = chain.request()

                val formBody = FormBody.Builder()
                        .add("grant_type", "refresh_token")
                        .add("refresh_token", refreshToken)
                        .build();

                val newRequest = originalRequest.newBuilder()
                        .addHeader("Authorization", "Basic " +
                                Base64.encodeToString((RedditAuthApi.CLIENT_ID + ":").toByteArray(),
                                        Base64.URL_SAFE or Base64.NO_WRAP))
                        .post(formBody)
                        .build()

                chain.proceed(newRequest)
            }).build()
        }
    }
}
