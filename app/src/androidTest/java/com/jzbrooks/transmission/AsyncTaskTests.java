package com.jzbrooks.transmission;

import android.support.test.runner.AndroidJUnit4;

import com.jzbrooks.transmission.data.Post;
import com.jzbrooks.transmission.data.source.RedditDataSource;
import com.jzbrooks.transmission.data.source.remote.api.FetchRedditPostsTask;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Async task tests for derived classes
 *
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class AsyncTaskTests {
    @Test
    public void fectchRedditDataTask() throws Exception {
        FetchRedditPostsTask task = new FetchRedditPostsTask();
        task.setCallback(new RedditDataSource.LoadPostsCallback() {
            @Override
            public void onPostsLoaded(List<Post> posts) {
                assertTrue(posts != null && posts.size() > 0);
            }

            @Override
            public void onDataNotAvailable() {
                fail("Remote endpoint returned no data.");
            }
        });
        task.execute();
        task.get();
    }
}
