package com.jzbrooks.transmission;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.jzbrooks.transmission.data.source.local.RedditContract;
import com.jzbrooks.transmission.data.source.local.RedditDatabaseHelper;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Async task tests for derived classes
 *
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class DatabaseTests {

    private Context mContext;

    public DatabaseTests() {
        mContext = InstrumentationRegistry.getTargetContext();
    }

    @Test
    public void testDatabaseCreation() throws Exception {
        setUp();
        
        // build a HashSet of all of the table names we wish to look for
        // Note that there will be another table in the DB that stores the
        // Android metadata (db version information)
        final HashSet<String> tableNameHashSet = new HashSet<String>();
        tableNameHashSet.add(RedditContract.PostEntry.TABLE_NAME);
        tableNameHashSet.add(RedditContract.SubredditEntry.TABLE_NAME);

        SQLiteDatabase db = new RedditDatabaseHelper(mContext).getWritableDatabase();
        assertEquals(true, db.isOpen());

        // have we created the tables we want?
        Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);

        assertTrue("Error: This means that the database has not been created correctly",
                c.moveToFirst());

        // verify that the tables have been created
        do {
            tableNameHashSet.remove(c.getString(0));
        } while( c.moveToNext() );

        // if this fails, it means that your database doesn't contain both the location entry
        // and weather entry tables
        assertTrue("Error: Your database was created without both the location entry and weather entry tables",
                tableNameHashSet.isEmpty());

        // now, do our tables contain the correct columns?
        c = db.rawQuery("PRAGMA table_info(" + RedditContract.PostEntry.TABLE_NAME + ")",
                null);

        assertTrue("Error: This means that we were unable to query the database for table information.",
                c.moveToFirst());

        // Build a HashSet of all of the column names we want to look for
        final HashSet<String> locationColumnHashSet = new HashSet<>();
        locationColumnHashSet.add(RedditContract.PostEntry._ID);
        locationColumnHashSet.add(RedditContract.PostEntry.COLUMN_TITLE);
        locationColumnHashSet.add(RedditContract.PostEntry.COLUMN_AUTHOR);
        locationColumnHashSet.add(RedditContract.PostEntry.COLUMN_SUBREDDIT_NAME);
        locationColumnHashSet.add(RedditContract.PostEntry.COLUMN_API_ID);

        int columnNameIndex = c.getColumnIndex("name");
        do {
            String columnName = c.getString(columnNameIndex);
            locationColumnHashSet.remove(columnName);
        } while(c.moveToNext());

        // if this fails, it means that your database doesn't contain all of the required location
        // entry columns
        assertTrue("Error: The database doesn't contain all of the required location entry columns",
                locationColumnHashSet.isEmpty());

        c.close();
        db.close();
    }

    @Test
    public void testPostTable() {
        setUp();

        insertTestSubredditEntry();

        insertTestPostEntry();
    }



    @Test
    public void testSubredditTable() {
        setUp();

        insertTestSubredditEntry();
    }

    @Test
    public void testFrontpageTable() {
        setUp();

        insertTestPostEntry();

        insertTestFrontpagePostEntry();
    }

    // helper methods
    private void setUp() {
        deleteDatabase(RedditContract.PostEntry.TABLE_NAME);
        deleteDatabase(RedditContract.SubredditEntry.TABLE_NAME);
        deleteDatabase(RedditContract.FrontpageEntry.TABLE_NAME);
        deleteDatabase(RedditContract.MessagesEntry.TABLE_NAME);
    }

    private void deleteDatabase(String databaseName) {
        mContext.deleteDatabase(databaseName);
    }

    private void insertTestPostEntry() {
        RedditDatabaseHelper databaseHelper = new RedditDatabaseHelper(mContext);
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(RedditContract.PostEntry.COLUMN_API_ID, "TEST_POST_API_ID_1r32");
        values.put(RedditContract.PostEntry.COLUMN_TITLE, "Test post title");
        values.put(RedditContract.PostEntry.COLUMN_AUTHOR, "crashtestdummy");
        values.put(RedditContract.PostEntry.COLUMN_COMMENT_COUNT, 4);
        values.put(RedditContract.PostEntry.COLUMN_CREATED, 1241233);
        values.put(RedditContract.PostEntry.COLUMN_DOMAIN, "self.test");
        values.put(RedditContract.PostEntry.COLUMN_NSFW, false);
        values.put(RedditContract.PostEntry.COLUMN_URL, "/r/TEST/21412/testposttitle");
        values.put(RedditContract.PostEntry.COLUMN_SCORE, 12);
        values.put(RedditContract.PostEntry.COLUMN_VISITED, true);
        values.put(RedditContract.PostEntry.COLUMN_SUBREDDIT_NAME, "testing");

        long postRowId = db.insert(RedditContract.PostEntry.TABLE_NAME, null, values);
        assertTrue(postRowId != -1);

        db.close();
    }

    private void insertTestSubredditEntry() {
        RedditDatabaseHelper databaseHelper = new RedditDatabaseHelper(mContext);
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(RedditContract.SubredditEntry.COLUMN_API_ID, "TEST_SUB_API_ID_4e32");
        values.put(RedditContract.SubredditEntry.COLUMN_SUBREDDIT_NAME, "TEST");

        long subredditRowId = db.insert(RedditContract.SubredditEntry.TABLE_NAME, null, values);
        assertTrue(subredditRowId != -1);

        db.close();
    }

    private void insertTestFrontpagePostEntry() {
        RedditDatabaseHelper databaseHelper = new RedditDatabaseHelper(mContext);
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(RedditContract.FrontpageEntry.COLUMN_API_ID, "TEST_POST_API_ID_1r32");

        long userRowId = db.insert(RedditContract.FrontpageEntry.TABLE_NAME, null, values);
        assertTrue(userRowId != -1);

        db.close();
    }
}

