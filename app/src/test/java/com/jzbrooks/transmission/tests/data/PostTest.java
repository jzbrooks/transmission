package com.jzbrooks.transmission.tests.data;

import com.jzbrooks.transmission.data.Post;

import junit.framework.Assert;

import org.junit.Test;

/**
 * Tests for the post model component of transmission
 */
public class PostTest {

    @Test
    public void builderTest() {

        // Arrange
        Post.Builder builder = new Post.Builder();

        String testId = "testId";
        String testTitle = "testTitle";
        String testSubreddit = "testSub";
        String testAuthor = "testAuthor";
        String testDomain = "google.com";
        String testUrl = "https://google.com";
        String testAfter = "testAfterId";
        long testCreated = 1;
        long testEdited = 2;
        long testCommentCount = 65536;
        long testScore = -1;
        boolean testIsVisited = true;
        boolean testIsOverEighteen = true;

        // Act
        builder.id(testId)
                .title(testTitle)
                .subreddit(testSubreddit)
                .author(testAuthor)
                .domain(testDomain)
                .url(testUrl)
                .after(testAfter)
                .created(testCreated)
                .edited(testEdited)
                .commentCount(testCommentCount)
                .score(testScore)
                .visited(testIsVisited)
                .overEighteen(testIsOverEighteen);

        Post post = builder.build();

        // Assert
        Assert.assertEquals(post.getId(), testId);
        Assert.assertEquals(post.getTitle(), testTitle);
        Assert.assertEquals(post.getSubreddit(), testSubreddit);
        Assert.assertEquals(post.getAuthor(), testAuthor);
        Assert.assertEquals(post.getDomain(), testDomain);
        Assert.assertEquals(post.getUrl(), testUrl);
        Assert.assertNull(post.getSelftext());
        Assert.assertNull(post.getSelftextHtml());
        Assert.assertEquals(post.getAfter(), testAfter);
        Assert.assertEquals(post.getCreated(), testCreated);
        Assert.assertEquals(post.getEdited(), testEdited);
        Assert.assertEquals(post.getCommentCount(), testCommentCount);
        Assert.assertEquals(post.getScore(), testScore);
        Assert.assertTrue(post.isVisited());
        Assert.assertTrue(post.isOverEighteen());
    }
}
