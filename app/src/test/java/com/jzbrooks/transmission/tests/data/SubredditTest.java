package com.jzbrooks.transmission.tests.data;

import com.jzbrooks.transmission.data.Subreddit;

import junit.framework.Assert;

import org.junit.Test;

/**
 * Tests for the subreddit model component of transmission
 */
public class SubredditTest {

    @Test
    public void builderTest() {

        // Arrange
        Subreddit.Builder builder = new Subreddit.Builder();

        String testId = "testId";
        String testName = "testName";

        // Act
        builder.id(testId)
                .name(testName);

        Subreddit subreddit = builder.build();

        // Assert
        Assert.assertEquals(subreddit.getId(), testId);
        Assert.assertEquals(subreddit.getName(), testName);
    }
}
