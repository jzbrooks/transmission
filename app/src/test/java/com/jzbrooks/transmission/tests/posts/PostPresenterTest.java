package com.jzbrooks.transmission.tests.posts;

import com.jzbrooks.transmission.data.Post;
import com.jzbrooks.transmission.data.User;
import com.jzbrooks.transmission.data.source.RedditDataSource;
import com.jzbrooks.transmission.data.source.RedditDataStore;
import com.jzbrooks.transmission.posts.PostsContract;
import com.jzbrooks.transmission.posts.PostsPresenter;

import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

/**
 * Tests for the post presenter component of transmission
 */
public class PostPresenterTest {

    @Test
    public void startTest() {

        // Arrange
        final String testPostTitle = "TestPostTitle";
        final Post post = new Post.Builder().title(testPostTitle).build();
        final List<Post> postList = new ArrayList<>(Collections.singletonList(post));

        RedditDataStore dataStore = mock(RedditDataStore.class);
        final User user = mock(User.class);
        PostsContract.View view = mock(PostsContract.View.class);

        doAnswer(
                new Answer<Void>() {
                    @Override
                    public Void answer(InvocationOnMock invocation) throws Throwable {
                        RedditDataSource.FetchUserCallback fetchUserCallback =
                                invocation.getArgument(0);

                        fetchUserCallback.onSuccess(user);
                        return null;
                    }
                }
        ).when(dataStore).getCurrentUser(any(RedditDataSource.FetchUserCallback.class));

        doAnswer(
                new Answer<Void>() {
                    @Override
                    public Void answer(InvocationOnMock invocation) throws Throwable {
                        RedditDataSource.LoadPostsCallback loadPostsCallback =
                                invocation.getArgument(3);

                        loadPostsCallback.onPostsLoaded(postList);
                        return null;
                    }
                }
        ).when(dataStore).getPosts(
                eq(user),
                (String) isNull(),
                anyString(),
                any(RedditDataSource.LoadPostsCallback.class)
        );

        PostsPresenter presenter = new PostsPresenter(dataStore, view);

        // Act
        presenter.start();

        // Assert
        Mockito.verify(dataStore).getPosts(
                eq(user),
                (String)isNull(),
                anyString(),
                any(RedditDataSource.LoadPostsCallback.class)
        );
        Mockito.verify(view).setPresenter(presenter);
        Mockito.verify(view).setLoadingIndicator(true);
        Mockito.verify(view).showPosts(postList);
    }

    @Test
    public void loadPostsTest() {

        // Arrange
        final String testPostTitle = "TestPostTitle";
        final Post post = new Post.Builder().title(testPostTitle).build();
        final List<Post> postList = new ArrayList<>(Collections.singletonList(post));

        RedditDataStore dataStore = mock(RedditDataStore.class);
        final User user = mock(User.class);
        PostsContract.View view = mock(PostsContract.View.class);

        doAnswer(
                new Answer<Void>() {
                    @Override
                    public Void answer(InvocationOnMock invocation) throws Throwable {
                        RedditDataSource.FetchUserCallback fetchUserCallback =
                                invocation.getArgument(0);

                        fetchUserCallback.onSuccess(user);
                        return null;
                    }
                }
        ).when(dataStore).getCurrentUser(any(RedditDataSource.FetchUserCallback.class));

        doAnswer(
                new Answer<Void>() {
                    @Override
                    public Void answer(InvocationOnMock invocation) throws Throwable {
                        RedditDataSource.LoadPostsCallback loadPostsCallback =
                                invocation.getArgument(3);

                        loadPostsCallback.onPostsLoaded(postList);
                        return null;
                    }
                }
        ).when(dataStore).getPosts(
                eq(user),
                (String) isNull(),
                anyString(),
                any(RedditDataSource.LoadPostsCallback.class)
        );

        PostsPresenter presenter = new PostsPresenter(dataStore, view);

        // Act
        presenter.loadPosts();

        // Assert
        Mockito.verify(dataStore).getPosts(
                eq(user),
                (String)isNull(),
                anyString(),
                any(RedditDataSource.LoadPostsCallback.class)
        );
        Mockito.verify(view).setPresenter(presenter);
        Mockito.verify(view).setLoadingIndicator(true);
        Mockito.verify(view).showPosts(postList);
    }

    @Test
    public void loadMorePostsTest() {

        // Arrange
        final String testPostTitle = "TestPostTitle";
        final Post post = new Post.Builder().title(testPostTitle).build();
        final List<Post> postList = new ArrayList<>(Collections.singletonList(post));

        RedditDataStore dataStore = mock(RedditDataStore.class);
        final User user = mock(User.class);
        PostsContract.View view = mock(PostsContract.View.class);

        doAnswer(
                new Answer<Void>() {
                    @Override
                    public Void answer(InvocationOnMock invocation) throws Throwable {
                        RedditDataSource.FetchUserCallback fetchUserCallback =
                                invocation.getArgument(0);

                        fetchUserCallback.onSuccess(user);
                        return null;
                    }
                }
        ).when(dataStore).getCurrentUser(any(RedditDataSource.FetchUserCallback.class));

        doAnswer(
                new Answer<Void>() {
                    @Override
                    public Void answer(InvocationOnMock invocation) throws Throwable {
                        RedditDataSource.LoadPostsCallback loadPostsCallback =
                                invocation.getArgument(3);

                        loadPostsCallback.onPostsLoaded(postList);
                        return null;
                    }
                }
        ).when(dataStore).getPosts(
                eq(user),
                (String) isNull(),
                anyString(),
                any(RedditDataSource.LoadPostsCallback.class)
        );

        PostsPresenter presenter = new PostsPresenter(dataStore, view);

        // Act
        presenter.loadMorePosts();

        // Assert
        Mockito.verify(dataStore).getPosts(
                eq(user),
                (String)isNull(),
                anyString(),
                any(RedditDataSource.LoadPostsCallback.class)
        );
        Mockito.verify(view).setPresenter(presenter);
        Mockito.verify(view).setLoadingIndicator(true);
        Mockito.verify(view).showMorePosts(postList);
    }
}
