package com.jzbrooks.transmission.tests.main;

import com.jzbrooks.transmission.data.Subreddit;
import com.jzbrooks.transmission.data.User;
import com.jzbrooks.transmission.data.source.RedditDataSource;
import com.jzbrooks.transmission.data.source.RedditDataStore;
import com.jzbrooks.transmission.main.MainContract;
import com.jzbrooks.transmission.main.MainPresenter;

import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

/**
 * Tests for the model presenter component of transmission
 */
public class MainPresenterTest {

    @Test
    public void getSubredditsTest() {

        // Arrange
        final String testSubredditName = "TestSubredditName";
        final Subreddit subreddit = new Subreddit.Builder().name(testSubredditName).build();
        final List<Subreddit> subredditList = new ArrayList<>(Collections.singletonList(subreddit));

        final User user = mock(User.class);

        RedditDataStore store = mock(RedditDataStore.class);
        MainContract.View view = mock(MainContract.View.class);

        MainPresenter presenter = new MainPresenter(store, view);

        doAnswer(
                new Answer() {
                    @Override
                    public Object answer(InvocationOnMock invocation) throws Throwable {
                        RedditDataSource.FetchUserCallback fetchUserCallback =
                                invocation.getArgument(0);

                        fetchUserCallback.onSuccess(user);
                        return null;
                    }
                }
        ).when(store).getCurrentUser(any(RedditDataSource.FetchUserCallback.class));

        doAnswer(
                new Answer() {
                    @Override
                    public Object answer(InvocationOnMock invocation) throws Throwable {
                        RedditDataSource.LoadSubredditsCallback loadSubredditsCallback =
                                invocation.getArgument(1);

                        loadSubredditsCallback.onSubredditsLoaded(subredditList);

                        return null;
                    }
                }
        ).when(store)
         .getSubreddits(eq(user), any(RedditDataSource.LoadSubredditsCallback.class));

        // Act
        presenter.getSubreddits();

        // Assert
        Mockito.verify(store)
                .getSubreddits(eq(user), any(RedditDataSource.LoadSubredditsCallback.class));
        Mockito.verify(view).showSubredditsView(subredditList);
    }

    @Test
    public void bottomMenuPostsClickedTest() {

        // Arrange
        RedditDataStore store = mock(RedditDataStore.class);
        MainContract.View view = mock(MainContract.View.class);

        MainPresenter presenter = new MainPresenter(store, view);

        // Act
        presenter.menuClicked(MainContract.MenuItem.Posts);

        // Assert
        Mockito.verify(view).showPostsView((String)isNull());
    }

    @Test
    public void bottomMenuMessagesClickedTest() {

        // Arrange
        RedditDataStore store = mock(RedditDataStore.class);
        MainContract.View view = mock(MainContract.View.class);

        MainPresenter presenter = new MainPresenter(store, view);

        // Act
        presenter.menuClicked(MainContract.MenuItem.Messages);

        // Assert
        Mockito.verify(view).showMessagesView();
    }

    @Test
    public void bottomMenuSubredditsClickedTest() {

        // Arrange
        RedditDataStore store = mock(RedditDataStore.class);
        MainContract.View view = mock(MainContract.View.class);

        MainPresenter presenter = spy(new MainPresenter(store, view));
        doNothing().when(presenter).getSubreddits();

        // Act
        presenter.menuClicked(MainContract.MenuItem.Subreddits);

        // Assert
        Mockito.verify(presenter).getSubreddits();
    }
}
