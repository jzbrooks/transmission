package com.jzbrooks.transmission.tests.data;

import com.jzbrooks.transmission.data.User;

import junit.framework.Assert;

import org.junit.Test;

/**
 * Tests for the user model component of transmission
 */
public class UserTest {

    @Test
    public void createUserTest() {

        // Arrange
        User user = new User();
        String testUsername = "username";
        String testAccessToken = "111b111";
        String testRefreshToken = "1b1b1b1";

        // Act
        user.setUsername(testUsername);
        user.setAccessToken(testAccessToken);
        user.setRefreshToken(testRefreshToken);

        // Assert
        Assert.assertEquals(user.getUsername(), testUsername);
        Assert.assertEquals(user.getAccessToken(), testAccessToken);
        Assert.assertEquals(user.getRefreshToken(), testRefreshToken);
    }
}
