package com.jzbrooks.transmission.tests.data.source;

import com.jzbrooks.transmission.data.source.RedditDataStore;
import com.jzbrooks.transmission.data.source.local.RedditLocalDataSource;
import com.jzbrooks.transmission.data.source.remote.RedditRemoteDataSource;

import junit.framework.Assert;

import org.junit.Test;

import static org.mockito.Mockito.mock;

/**
 * Tests for the data store component of transmission
 */
public class RedditDataStoreTest {

    @Test
    public void getInstanceTest() {

        RedditRemoteDataSource remoteDataSource = mock(RedditRemoteDataSource.class);
        RedditLocalDataSource localDataSource = mock(RedditLocalDataSource.class);

        RedditDataStore store = RedditDataStore.getInstance(remoteDataSource, localDataSource);

        Assert.assertNotNull(store);
    }
}
